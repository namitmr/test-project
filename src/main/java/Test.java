
import org.postgresql.util.PSQLException;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.exceptions.JedisException;
import utils.CommonUtils;

import java.sql.*;
import java.util.Date;
import java.util.*;

/**
 * Created by namitmahuvakar on 18/04/17.
 */
    /*
    You can compile and run this example with a command like:
      javac BasicSample.java && java -cp .:~/path/to/postgresql-9.4.1208.jar BasicSample
    You can download the postgres JDBC driver jar from https://jdbc.postgresql.org.
    */
public class Test {

    public static final String[] experimentIds = new String[]{"exp_1", "exp_2", "exp_3"};
    public static final String[] eventNames = new String[]{"email_delivered", "email_open", "email_click"};
    public static final String[] campaignEventDates = new String[]{"20170301", "20170302", "20170303"};
    public static org.slf4j.Logger logger = LoggerFactory.getLogger(Test.class);
    private static final String redisHost = "localhost";
    private static final Integer redisPort = 6379;

    private static Test self = null;
    private static JedisPool jedisPool = null;
    private static Jedis jedis = null;
//    private AtomicBoolean isShuttingDown = new AtomicBoolean(false);
//    private CountDownLatch waitForWorkToCompleteLatch = new CountDownLatch(1);

//    public Test() {
//        JedisPoolConfig config = new JedisPoolConfig();
//        config.setMaxWaitMillis(60000);
//        jedisPool = new JedisPool(config, redisHost, redisPort);
//    }
//
//    public static Test get() {
//        if(self == null) {
//            synchronized (Test.class) {
//                if(self == null) {
//                    self = new Test();
//                }
//            }
//        }
//
//        return self;
//    }

    public static void main(String[] args) {
        int requiredNumParams = 7;
        if(args.length < requiredNumParams) {
            logger.error("Required args: 7 <int numlicenseCode, String campaignsPrefix, int campaignStartRunNumber, int maxCampaignRunNumber, int totalRunsPerCampaign, int maxCampaignDays, int numUsers>");
            System.exit(1);
        }

        int numlicenseCode = Integer.parseInt(args[0]);
        String campaignsPrefix = args[1];
        int campaignStartRunNumber = Integer.parseInt(args[2]);
        int maxCampaignRunNumber = Integer.parseInt(args[3]);
        int totalRunsPerCampaign = Integer.parseInt(args[4]);
        int maxCampaignDays = Integer.parseInt(args[5]);
        int numUsers = Integer.parseInt(args[6]);

        logger.info("running_campaign_benchmarks_with_supplied_params",
                numlicenseCode, campaignsPrefix, campaignStartRunNumber, maxCampaignRunNumber, totalRunsPerCampaign, maxCampaignDays, numUsers);

        runCampaigns(numlicenseCode, campaignsPrefix, campaignStartRunNumber, maxCampaignRunNumber, totalRunsPerCampaign, maxCampaignDays, numUsers);
    }



    public static void runCampaigns(int numlicenseCode, String campaignsPrefix, int campaignStartRunNumber, int maxCampaignRunNumber, int totalRunsPerCampaign, int maxCampaignDays, int numUsers){
        String lc = "", expId = "", scopeId = "";
        int value = 1, valueScope =1, runs = totalRunsPerCampaign;
        Date x, y;
        ResultSet res;
        Boolean flag = true, eventSent = false;
        HashMap<String, Integer> lcUsers = new HashMap<>();
        HashMap<String, Integer> temp = new HashMap<>();
        HashMap<String, HashMap<String, Integer>> lcScopes = new HashMap<>();
        long date = System.currentTimeMillis();
        try {
            Class.forName("org.postgresql.Driver");
            Connection db = DriverManager.getConnection("jdbc:postgresql://192.168.2.169:26257,localhost:26258,localhost:26259/users_list?sslmode=disable", "root", "");
            JedisPoolConfig config = new JedisPoolConfig();
            config.setMaxWaitMillis(60000);
//            jedisPool = new JedisPool(config, redisHost, redisPort);
//            jedis = jedisPool.getResource();
            eventSent = false;
            for (int i = 1; i <= numUsers; i++) {
                if(i%10000 == 0){
                    logger.info(i+" Users in :"+(date-(System.currentTimeMillis())));
                }
                lc = "lc-" + (1 + (int) (Math.random() * ((numlicenseCode - 1) + 1)));
                expId = "expId-" + (campaignStartRunNumber + (int) (Math.random() * ((maxCampaignRunNumber - campaignStartRunNumber) + 1))) +"-"+ lc;
                while (runs > 0) {
                    scopeId = "scope-" + DataUtils.randomScope();
                    if(flag == true) {
                        if (lcUsers.containsKey(lc)) {
                            lcUsers.put(lc, lcUsers.get(lc) + 1);
                            value = lcUsers.get(lc);
                        } else {
                            lcUsers.put(lc, 1);
                        }
                        insertCockroachUsers(db, lc, DataUtils.generateUserId(i), value);
                        flag = false;
                    }


                    if(false == lcScopes.containsKey(lc)) {
                        lcScopes.put(lc, new HashMap<String, Integer>());
                    }


                    temp = lcScopes.get(lc);
                    if(false == temp.containsKey(expId)) {
                        temp.put(expId, 1);
                    } else {
                        valueScope = temp.get(expId) + 1;
                        temp.put(expId, valueScope);
                    }


                    insertCockroachScope(db, lc, expId, scopeId, valueScope);
//                    if (i % 7 == 0 && eventSent == false) {
//                        setBits(DataUtils.generateCampaignEventHashKey(lc, expId, eventNames[0], CommonUtils.getRandomDateBetweenGiven(new java.util.Date(), maxCampaignDays, true)), value);
//                        setBits(DataUtils.generateScopedCampaignEventHashKey(lc, expId, eventNames[0], CommonUtils.getRandomDateBetweenGiven(new java.util.Date(), maxCampaignDays, true)), value);
//                        eventSent = true;
//                    }
//                    if (i % 13 == 0 && eventSent == false) {
//                        x = CommonUtils.getRandomDateBetweenGiven(new java.util.Date(), maxCampaignDays, true);
//                        setBits(DataUtils.generateCampaignEventHashKey(lc, expId, eventNames[0], x), value);
//                        setBits(DataUtils.generateScopedCampaignEventHashKey(lc, expId, eventNames[0], x), value);
//                        x = CommonUtils.getRandomDateBetweenGiven(x, maxCampaignDays, true);
//                        setBits(DataUtils.generateCampaignEventHashKey(lc, expId, eventNames[1], x), value);
//                        setBits(DataUtils.generateScopedCampaignEventHashKey(lc, expId, eventNames[1], x), value);
//                        eventSent = true;
//                    }
//                    if (i % 23 == 0 && eventSent == false) {
//                        x = CommonUtils.getRandomDateBetweenGiven(new java.util.Date(), maxCampaignDays, true);
//                        setBits(DataUtils.generateCampaignEventHashKey(lc, expId, eventNames[0], x), value);
//                        setBits(DataUtils.generateScopedCampaignEventHashKey(lc, expId, eventNames[0], x), value);
//                        y = CommonUtils.getRandomDateBetweenGiven(x, maxCampaignDays, true);
//                        setBits(DataUtils.generateCampaignEventHashKey(lc, expId, eventNames[1], y), value);
//                        setBits(DataUtils.generateScopedCampaignEventHashKey(lc, expId, eventNames[1], y), value);
//                        x = CommonUtils.getRandomDateBetweenGiven(y, maxCampaignDays, true);
//                        setBits(DataUtils.generateCampaignEventHashKey(lc, expId, eventNames[2], x), value);
//                        setBits(DataUtils.generateScopedCampaignEventHashKey(lc, expId, eventNames[2], x), value);
//                    }
                    runs --;
                    eventSent = false;
                }
                runs = totalRunsPerCampaign;
                value = 1;
                valueScope = 1;
                flag = true;
                try {
                    res = db.createStatement().executeQuery("select * from users_list.Users  where lc = 'lc-" + (1 + (int) (Math.random() * ((numlicenseCode - 1) + 1))) + "' and cuid = '" + DataUtils.generateUserId(1 + (int) (Math.random() * ((numUsers - 1) + 1))) + "'");
                    res = db.createStatement().executeQuery("select * from users_list.Scopes  where lc = 'lc-" + (1 + (int) (Math.random() * ((numlicenseCode - 1) + 1))) +
                            "' and experiment_id = 'expId-" + DataUtils.generateUserId(1 + (int) (Math.random() * ((numUsers - 1) + 1))) + "-lc-" +
                            (1 + (int) (Math.random() * ((numlicenseCode - 1) + 1))) + "'");
                }catch (Exception e){
                    logger.error(e.getMessage());
                }
            }
        }catch (ClassNotFoundException e){
            logger.error(e.getMessage());
            System.exit(1);
        } catch (SQLException e) {
            logger.error(e.getMessage());
            System.exit(1);
        }


    }



    public static void insertCockroachUsers(Connection db,String lc, String str, Integer bitval ) throws ClassNotFoundException, SQLException {
                try {
                    db.createStatement().execute("INSERT INTO users_list.Users (lc, cuid, bitIndex) VALUES ('"+lc+"','" + str + "', " + bitval + ")");
                } catch (PSQLException e) {
                    logger.error(e.getMessage());
                }
//                if(i % 5 == 0) {
//                    str1 = (1 + (int)(Math.random() * ((1000000 - 1) + 1))) +":"+(1 + (int)(Math.random() * ((10000 - 1) + 1)));
//                    try {
//                        res =  db.createStatement().executeQuery("select * from users_list.kv where k='" + str1 + "'");
//                    }catch (PSQLException e) {
//                        logger.error(e.getMessage());
//                        continue;
//                    }
//                }
    }
    public static void insertCockroachScope(Connection db,String lc, String expId, String scopeId , Integer bitval ) throws ClassNotFoundException, SQLException {
        try {
            db.createStatement().execute("INSERT INTO users_list.Scopes (lc, experiment_id, scope_id , bitIndex) VALUES ('"+lc+"','" + expId + "','"+scopeId+"', " + bitval + ")");
        } catch (PSQLException e) {
            logger.error(e.getMessage());
        }
    }

    public static void setBits(String key, int count) {
        try{
            long t1 = System.currentTimeMillis();
//
//            for(int i = 0; i < count; i++){
//                if(i % 10000 == 0) {
//                    logger.info("setted: " + i);
//                }
            logger.info("setted: " + key+":"+count);
                jedis.setbit(key, count, true);
//            }

            logger.info("Time taken", (System.currentTimeMillis() - t1)) ;
        } catch (JedisException e) {
            logger.error(e.getMessage(), e);
        }
    }

    public void shutdown(){
        jedisPool.destroy();
    }


}