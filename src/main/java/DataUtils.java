
import java.math.BigInteger;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;


public class DataUtils {
    private static final Random r = new Random();
    static final Random secureRandom = new SecureRandom();

    private static final int SCOPE_LENGTH = 20;

    public static final String[] experimentIds = new String[]{"exp_1", "exp_2", "exp_3"};
    public static final String[] eventNames = new String[]{"email_delivered", "email_open", "email_click"};
    public static final String[] campaignEventDates = new String[]{"20170301", "20170302", "20170303"};
    //public static final String[] eventNames = new String[]{"email_accepted", "email_accpeted", "email_attempt", "email_bounce", "email_click", "email_deferred", "email_delivered", "email_dropped", "email_open", "email_processed", "email_rejected", "email_resubscribe", "email_sent", "email_spam", "email_unsubscribe", "email_invalid"};
    //public static final String[] campaignEventDates = new String[]{"20170301", "20170302", "20170303", "20170304", "20170305", "20170306", "20170307", "20170308", "20170309", "20170310", "20170311", "20170312", "20170313", "20170314", "20170315", "20170316", "20170317", "20170318", "20170319", "20170320", "20170321", "20170322", "20170323", "20170324", "20170325", "20170326", "20170327", "20170328", "20170329", "20170330"};

    public static final int experimentIdsLength = experimentIds.length;
    public static final int eventNamesLength = eventNames.length;
    public static final int campaignEventDatesLength = campaignEventDates.length;

    private static final ThreadLocal<SimpleDateFormat> dateFormatter = new ThreadLocal<SimpleDateFormat>() {
        @Override
        protected SimpleDateFormat initialValue() {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            format.setTimeZone(TimeZone.getTimeZone("UTC"));

            return format;
        }
    };

    public static String formatToDateString(Date date){
        return dateFormatter.get().format(date);
    }

    public static String generateUsersHashStoreKey(String licenseCode){
        return licenseCode + ":" + "users";
    }

    public static String generateUserId(int idx){
        return "someOkOkLongUserId:" + idx;
    }

    public static String generateScopesHashStoreKey(String licenseCode, String experimentId){
        return licenseCode + ":" + experimentId + ":" + "scopes";
    }

    public static String generateCampaignEventHashKey(String licenseCode, String experimentId, String eventName, Date date){
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        return licenseCode + ":" + experimentId + ":" + eventName + ":" + dateFormat.format(date);
    }

    public static String generateRandomCampaignEventHashKey(String licenseCode){
        return licenseCode
                + ":" + experimentIds[r.nextInt(experimentIdsLength)]
                + ":" + eventNames[r.nextInt(eventNamesLength)]
                + ":" + campaignEventDates[r.nextInt(campaignEventDatesLength)];
    }

    public static String generateScopedCampaignEventHashKey(String licenseCode, String experimentId, String eventName, Date date){
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        return licenseCode + ":" + experimentId + ":" + eventName + ":" + dateFormat.format(date)+":scoped";
    }

    public static String randomKey(int length) {
        return String.format("%" + length + "s"
                , new BigInteger(length * 5/*base 32,2^5*/, secureRandom)
                        .toString(32)).replace('\u0020', '0');
    }

    public static String randomScope() {
        return randomKey(SCOPE_LENGTH);
    }

    public static String generateScope(String licenseCode, String experimentId, String userId, int runNumber) {
        return "scope" + ":" + licenseCode + ":" + experimentId + ":" + userId + ":" + runNumber;
    }

    public static float[] generateEventsFlowProbabilities(String[] eventNames, float baseProbability, float flowProbabilityDivisor){
        float[] eventsFlowProbabilities = new float[eventNames.length];

        for(int i = 0; i < eventNames.length; i++){
            if(i == 0) {
                eventsFlowProbabilities[i] = baseProbability;
            } else {
                eventsFlowProbabilities[i] = eventsFlowProbabilities[i - 1] / flowProbabilityDivisor;
            }
        }

        return eventsFlowProbabilities;
    }

    public static int[] generateEventsFlowProbabilities(int count, int eventsFunnelStepDivisorBase){
        int[] eventsFlowProbabilities = new int[count];
        eventsFlowProbabilities[0] = eventsFunnelStepDivisorBase;

        for(int i = 1; i < count; i++){
            eventsFlowProbabilities[i] = eventsFlowProbabilities[i - 1] * 2;
        }

        return eventsFlowProbabilities;
    }
}
