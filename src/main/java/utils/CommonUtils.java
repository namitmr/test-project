package utils;

import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;

public class CommonUtils {
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(CommonUtils.class);

    private static final Random r = new Random();

    public static Calendar getUTCCalendar(){
        return Calendar.getInstance(TimeZone.getTimeZone("UTC"));
    }

    public static boolean getProbabilityOutcome(float maxProbabilityForThis){
        int intProbability = (int) (maxProbabilityForThis * 100);
        int curProbability = r.nextInt(100) + 1;

        return intProbability >= curProbability;
    }

    /**
     * Note: Month is ZERO based
     */
    public static Date getDateFromValues(int year, int month, int day){
        Calendar cal = CommonUtils.getUTCCalendar();
        cal.set(year, month, day, 0, 0, 0);
        return cal.getTime();
    }

    /**
     * Note: Month is ZERO based
     */
    public static Date[] getStartAndEndDates(int startYear, int startMonth, int startDay, int endYear, int endMonth, int endDay){
        Date startDate = getDateFromValues(startYear, startMonth, startDay);
        Date endDate = getDateFromValues(endYear, endMonth, endDay);

        return new Date[]{startDate, endDate};
    }

    public static void iterateBetweenTwoDates(){
        Date[] startAndEndDates = CommonUtils.getStartAndEndDates(2016, 0, 1, 2016, 1, 1);

        Calendar start = CommonUtils.getUTCCalendar();
        start.setTime(startAndEndDates[0]);

        Calendar end = CommonUtils.getUTCCalendar();
        end.setTime(startAndEndDates[1]);

        for (Date date = start.getTime(); start.before(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
            logger.info(date.toString());
        }
    }

    public static Date getRandomDateBetweenGiven(Date baseDate, int maxDaysInFuture, boolean includeCurrentDay){
        if(baseDate == null) {
            baseDate = new Date();
        }

        int randomDayInFuture = 0;

        if(includeCurrentDay == true) {
            randomDayInFuture = r.nextInt(maxDaysInFuture + 1);
        } else {
            randomDayInFuture = r.nextInt(maxDaysInFuture) + 1;
        }

        Calendar baseDateCalendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        baseDateCalendar.setTime(baseDate);
        baseDateCalendar.add(Calendar.SECOND, randomDayInFuture * 86400);

        return baseDateCalendar.getTime();
    }

    public static Date getNextDay(Date baseDate){
        if(baseDate == null) {
            baseDate = new Date();
        }

        Calendar baseDateCalendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        baseDateCalendar.setTime(baseDate);
        baseDateCalendar.add(Calendar.SECOND, 86400);

        return baseDateCalendar.getTime();
    }

    public static void createDirectories(String filePath) throws IOException {
        filePath = filePath + File.separator + "dummy.txt";

        File file = new File(filePath);
        if(!file.exists()){
            file.getParentFile().mkdirs();
        }
    }
}
